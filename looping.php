<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Looping</h1>    
    <?php
    echo "<h3>Soal 1</h3>";
    echo "<h4>Looping 1</h4>";
    for($i=2; $i<=20; $i+=2){
        echo $i. " - I Love PHP <br>";
    }

    echo "<h4>Looping 2</h4>";
    for($u=20; $u>=2; $u-=2){
        echo $u. " - I Love PHP <br>";
    }

    echo "<h3>Soal 2</h3>";
    $numbers = [18,45,29,61,47,34];
    echo "Array Numbers : " ;
    print_r($numbers);
    echo "<br>";
    foreach($numbers as $no){
        $rest[] = $no %=5;
    }

    echo "Array sisa baginya adalah:  "; 
    print_r($rest);
    echo "<br>";

    echo "<h3>Soal 3</h3>";
    $items = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach($items as $key => $value){
        $item = array(
            "ID" => $value[0],
            "Name" => $value[1],
            "Price" => $value[2],
            "Description" => $value[3],
            "Source" => $value[4]
        );
        print_r($item);
        echo "<br>";
    } 

    echo "<h3>Soal 4</h3>";
    for($j=1; $j<=5; $j++){
        for($k=1; $k<=$j; $k++){
        echo "*";
        }
        echo "<br>";
    }


    ?>
</body>
</html>